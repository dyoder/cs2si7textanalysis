package edu.westga.cs1302.textanalysis;

import edu.westga.cs1302.textanalysis.controller.TextAnalysisController;

/**
 * Entry point for the program.
 * 
 * @author CS 1302
 */
public class Main {

	/**
	 * Default entry point for the program.
	 * 
	 * @param args
	 *            command line arguments for the program
	 */
	public static void main(String[] args) {
		TextAnalysisController demo = new TextAnalysisController();
		demo.run();
	}

}
